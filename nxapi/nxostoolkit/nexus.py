import requests
import json


class nexus:
    """Requirements:
            - Python 3.6+
            - Libraries needed:
                    - requests
                    - json
    """

    def __init__(self, user, pswd, url):
        """Initializes the Nexus module with the following parameters:

        Attributes:
                user     (str):  Contains the username used in the session
                pswd     (str):  Contains the password used in the session
                url      (str):  Contains the url of the device being configured
                platform (str):  Hardware description of the device being configured
                version  (str):  Version of the Firmware running in the device

        Args:
                user (str): The name of the user to access the NX-API
                pswd (str): The password of the user to access the NX-API
                url  (str): The url of the device we want to interact with

        Returns:
                (object) A Nexus object that let's the user interact with the device using the following methods.
        """

        self.user = user
        self.pswd = pswd
        self.url = url

        platform_headers = {'content-type': 'application/json'}

        platform_body = {
            "ins_api": {
                "version": "1.0",
                "type": "cli_show",
                "chunk": "0",
                "sid": "1",
                "input": "show version",
                "output_format": "json"
            }
        }

        platform_response = requests.post(url,
                                          data=json.dumps(platform_body),
                                          headers=platform_headers,
                                          auth=(user, pswd)).json()

        self.platform = platform_response["ins_api"]["outputs"]["output"]["body"]["chassis_id"]
        self.version = platform_response["ins_api"]["outputs"]["output"]["body"]["kickstart_ver_str"]

    def authenticate(self, user, pswd):
        """Function that authenticates an user with the given user and
        password in the device using AAA authentication

        Args:
                user (str): Name of the user that wants to authenticate
                pswd (str): Password of the user that wants to authenticate
        Returns:
                (None) Nothing

        """
        aaa_url = self.url + '/api/aaaLogin.json'

        aaa_headers = {'content-type': 'application/json'}
        
        aaa_body = {
            "aaaUser": {
                "attributes": {
                    "name": user,
                    "pwd": pswd
                }
            }
        }

        response = requests.post(aaa_url,
                data=json.dumps(aaa_body),
                headers=aaa_headers,
                auth=(user, pswd)).json()

    def get_interface_status(self, if_name):
        """Function used to obtain the status of the interface passed as an
        argument.

        Args:
                if_name (str): Name of the interface in the device we want to know the
                version of.

        Returns:
                (str) The status of the given interface, it can be one of the following:
                        - Notconnected
                        - Unknown
                        - Up
                        - Down
        """
        int_status_headers = {'content-type': 'application/json'}
        int_status_body = {
            "ins_api": {
                "version": "1.0",
                "type": "cli_show",
                "chunk": "0",
                "sid": "1",
                "input": "show interface " + if_name + " status",
                "output_format": "json"
            }
        }
        response = requests.post(self.url,
                                 data=json.dumps(int_status_body),
                                 headers=int_status_headers,
                                 auth=(self.user, self.pswd)).json()

        return response["ins_api"]["outputs"]["output"]["body"]["TABLE_interface"]["ROW_interface"]["state"]

    def configure_interface_desc(self, if_name, description):
        """This function sets a description to an Interface in the device.
        Args:
                if_name     (str): Interface id of the switch we want to modify the description.
                description (str): Description we want to set to the switch

        Returns:
                (None) Nothing
        """
        conf_int_desc_headers = {'content-type': 'application/json'}

        conf_int_desc_body = {
            "ins_api": {
                "version": "1.0",
                "type": "cli_conf",
                "chunk": "0",
                "sid": "1",
                "input": "interface " +
                if_name +
                " ; description " +
                description,
                "output_format": "json"}}

        response = requests.post(self.url,
                                 data=json.dumps(conf_int_desc_body),
                                 headers=conf_int_desc_headers,
                                 auth=(self.user, self.pswd)).json()